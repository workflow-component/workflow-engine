package com.tbyacoub.workflow.engine.factory;

import com.tbyacoub.workflow.engine.execute.WorkflowStepExecutor;
import com.tbyacoub.workflow.engine.execute.executors.ExpressionExecutor;
import com.tbyacoub.workflow.engine.execute.executors.ListExecutor;
import com.tbyacoub.workflow.engine.execute.executors.RegexExecutor;
import com.tbyacoub.workflow.engine.execute.predicate.ListPredicates;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;
import lombok.Getter;

@Getter
public class WorkflowStepExecutorFactory<T> {

    private final Class<T> clazz;

    private final WorkflowStepMetadata.Type type;

    private final ListPredicates<?> listPredicates;

    public WorkflowStepExecutorFactory(
            Class<T> clazz,
            WorkflowStepMetadata.Type type,
            ListPredicates<?> listPredicates) {
        this.clazz = clazz;
        this.type = type;
        this.listPredicates = listPredicates;
    }

    /**
     * Use the factory to get the next instance.
     */
    public WorkflowStepExecutor<T> getInstance() {
        if (type == WorkflowStepMetadata.Type.EXPRESSION) {
            return new ExpressionExecutor<>();
        } else if (type == WorkflowStepMetadata.Type.LIST) {
            //noinspection unchecked
            return new ListExecutor<>((ListPredicates<T>) listPredicates);
        } else if (type == WorkflowStepMetadata.Type.REGEX) {
            return new RegexExecutor<>();
        }

        return null;
    }

    public static <V> WorkflowStepExecutorFactory<V> getInstance(Class<V> clazz, WorkflowStepMetadata.Type type, ListPredicates<?> listPredicates) {
        return new WorkflowStepExecutorFactory<>(clazz, type, listPredicates);
    }
}
