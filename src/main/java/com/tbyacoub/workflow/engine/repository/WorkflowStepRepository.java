package com.tbyacoub.workflow.engine.repository;

import com.tbyacoub.workflow.engine.model.WorkflowStep;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkflowStepRepository extends CrudRepository<WorkflowStep, Long> {
}
