package com.tbyacoub.workflow.engine.converter.jpa;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;
import lombok.SneakyThrows;

import javax.persistence.AttributeConverter;

public class WorkflowStepMetadataConverter implements AttributeConverter<WorkflowStepMetadata, String> {

    @SneakyThrows
    @Override
    public String convertToDatabaseColumn(WorkflowStepMetadata attribute) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(attribute);
    }

    @SneakyThrows
    @Override
    public WorkflowStepMetadata convertToEntityAttribute(String dbData) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(dbData, WorkflowStepMetadata.class);
    }
}
