package com.tbyacoub.workflow.engine.converter;

import com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepTestMessage;
import com.tbyacoub.workflow.engine.model.WorkflowStepTest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class WorkflowStepTestMessageConverter implements Converter<WorkflowStepTestMessage, WorkflowStepTest> {

    private final WorkflowStepMessageConverter workflowStepMessageConverter;

    public WorkflowStepTestMessageConverter(WorkflowStepMessageConverter workflowStepMessageConverter) {
        this.workflowStepMessageConverter = workflowStepMessageConverter;
    }

    @Override
    public WorkflowStepTest convert(WorkflowStepTestMessage source) {
        WorkflowStepTest workflowStepTest = new WorkflowStepTest();
        workflowStepTest.setWorkflowStep(workflowStepMessageConverter.convert(source.getWorkflowStepMessage()));
        workflowStepTest.setTestObject(source.getTestObject());

        return workflowStepTest;
    }
}
