package com.tbyacoub.workflow.engine.converter;

import com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepProcessorResultMessage;
import com.tbyacoub.workflow.engine.process.WorkflowStepProcessorResult;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class WorkflowStepProcessorResultConverter implements Converter<WorkflowStepProcessorResult, WorkflowStepProcessorResultMessage> {

    @Override
    public WorkflowStepProcessorResultMessage convert(WorkflowStepProcessorResult source) {
        return WorkflowStepProcessorResultMessage
                .newBuilder()
                .setTypeValue(source.getType().ordinal())
                .setResult(source.isResult())
                .setFieldValue(String.valueOf(source.getFieldValue()))
                .setErrorMessage(source.getErrorMessage() == null ? "" : source.getErrorMessage())
                .build();
    }
}
