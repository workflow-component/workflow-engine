package com.tbyacoub.workflow.engine.converter;

import com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMetadataMessage;
import com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage;
import com.tbyacoub.workflow.engine.model.WorkflowStep;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class WorkflowStepConverter implements Converter<WorkflowStep, WorkflowStepMessage> {


    @Override
    public WorkflowStepMessage convert(WorkflowStep source) {

        WorkflowStepMetadataMessage workflowStepMetadataMessage = WorkflowStepMetadataMessage
                .newBuilder()
                .setExpression(source.getMetadata().getArgument())
                .setTypeValue(source.getMetadata().getType().ordinal())
                .build();

        return WorkflowStepMessage
                .newBuilder()
                .setId(source.getId())
                .setMetadata(workflowStepMetadataMessage)
                .setFieldPath(source.getFieldPath())
                .build();
    }
}
