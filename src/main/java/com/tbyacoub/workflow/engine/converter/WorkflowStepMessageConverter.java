package com.tbyacoub.workflow.engine.converter;

import com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage;
import com.tbyacoub.workflow.engine.model.WorkflowStep;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class WorkflowStepMessageConverter implements Converter<WorkflowStepMessage, WorkflowStep> {

    @Override
    public WorkflowStep convert(WorkflowStepMessage source) {
        WorkflowStep workflowStep = new WorkflowStep();

        WorkflowStepMetadata workflowStepMetadata = new WorkflowStepMetadata();
        workflowStepMetadata.setArgument(source.getMetadata().getExpression());
        workflowStepMetadata.setType(WorkflowStepMetadata.Type.valueOf(source.getMetadata().getType().name()));

        workflowStep.setMetadata(workflowStepMetadata);
        workflowStep.setFieldPath(source.getFieldPath());
        workflowStep.setClientId(0L);

        return workflowStep;
    }
}
