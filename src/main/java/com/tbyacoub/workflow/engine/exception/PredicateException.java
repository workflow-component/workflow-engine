package com.tbyacoub.workflow.engine.exception;

public class PredicateException extends RuntimeException {

    public PredicateException(String errorMessage) {
        super(errorMessage);
    }
}
