package com.tbyacoub.workflow.engine.exception;

public class WorkflowStepExecutionException extends RuntimeException {

    public WorkflowStepExecutionException(String errorMessage) {
        super(errorMessage);
    }
}
