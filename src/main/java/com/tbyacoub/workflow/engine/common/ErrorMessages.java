package com.tbyacoub.workflow.engine.common;

public class ErrorMessages {

    public static class WorkflowStepExecution {

        public static final String FIELD_IS_NOT_PRIMITIVE = "Field is not a primitive";

        public static final String EXPRESSION_RESULT_NOT_BOOLEAN = "Expression execution result is not boolean";

        public static final String NON_STRING_FIELD = "Field is not a string";

        public static final String ONE_ARGUMENT_REQUIRED = "List Excecutor requires at least one argument";

        public static final String NO_SUCH_METHOD = "No such method exists";

        public static final String METHOD_INVOCATION_EXCEPTION = "Method invocation exception";

        public static final String PREDICATE_ARGUMENT_SIZE = "Predicate requires %s argument";

        public static final String PREDICATE_ARGUMENT_SHAPE = "Predicate requires %s argument shape";

        public static final String REGEX_ARGUMENT_IS_EMPTY = "Regex cannot be empty";
    }
}
