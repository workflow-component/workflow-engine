package com.tbyacoub.workflow.engine.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class WorkflowStepMetadata {

    public enum Type {
        EXPRESSION,
        REGEX,
        LIST
    }

    private Type type;

    @NotEmpty
    private String argument;
}
