package com.tbyacoub.workflow.engine.model;

import com.tbyacoub.workflow.engine.converter.jpa.WorkflowStepMetadataConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@Entity
@Getter
@Setter
public class WorkflowStep {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Valid
    @Column(columnDefinition = "JSON")
    @Convert(converter = WorkflowStepMetadataConverter.class)
    private WorkflowStepMetadata metadata;

    @NotEmpty
    private String fieldPath;

    private Long clientId;

}
