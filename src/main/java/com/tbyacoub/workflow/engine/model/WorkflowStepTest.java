package com.tbyacoub.workflow.engine.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkflowStepTest {

    private WorkflowStep workflowStep;

    private String testObject;
}
