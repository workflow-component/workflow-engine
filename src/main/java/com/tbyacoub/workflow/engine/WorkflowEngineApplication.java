package com.tbyacoub.workflow.engine;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@SpringBootApplication
public class WorkflowEngineApplication {

    @RequestMapping("/test")
    @PreAuthorize("hasAuthority('SCOPE_workflow_engine/workflow_step')")
    public String test() {
        log.info("here");
        return "Hello World";
    }

    public static void main(String[] args) {
        SpringApplication.run(WorkflowEngineApplication.class, args);
    }

}
