package com.tbyacoub.workflow.engine.service;

import com.tbyacoub.workflow.engine.model.WorkflowStep;
import com.tbyacoub.workflow.engine.repository.WorkflowStepRepository;

import java.util.List;
import java.util.Optional;

public class JpaWorkflowStepService implements WorkflowStepService {

    private final WorkflowStepRepository workflowStepRepository;

    public JpaWorkflowStepService(
            WorkflowStepRepository workflowStepRepository) {
        this.workflowStepRepository = workflowStepRepository;
    }

    @Override
    public WorkflowStep save(WorkflowStep workflowStep) {
        return workflowStepRepository.save(workflowStep);
    }

    @Override
    public Optional<WorkflowStep> find(Long id) {
        return workflowStepRepository.findById(id);
    }

    @Override
    public Iterable<WorkflowStep> findAllById(List<Long> ids) {
        return workflowStepRepository.findAllById(ids);
    }

    @Override
    public Iterable<WorkflowStep> findAll() {
        return workflowStepRepository.findAll();
    }
}
