package com.tbyacoub.workflow.engine.service;

import com.tbyacoub.workflow.engine.model.WorkflowStep;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.Optional;

public interface WorkflowStepService {

    WorkflowStep save(@Validated WorkflowStep workflowStep);

    Optional<WorkflowStep> find(Long id);

    Iterable<WorkflowStep> findAllById(List<Long> ids);

    Iterable<WorkflowStep> findAll();
}
