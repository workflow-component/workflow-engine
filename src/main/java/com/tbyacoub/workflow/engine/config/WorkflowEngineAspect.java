package com.tbyacoub.workflow.engine.config;

import com.tbyacoub.workflow.engine.annotation.PredicateArgumentShape;
import com.tbyacoub.workflow.engine.annotation.PredicateArgumentSize;
import com.tbyacoub.workflow.engine.exception.PredicateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.Arrays;

import static com.tbyacoub.workflow.engine.common.ErrorMessages.WorkflowStepExecution.PREDICATE_ARGUMENT_SHAPE;
import static com.tbyacoub.workflow.engine.common.ErrorMessages.WorkflowStepExecution.PREDICATE_ARGUMENT_SIZE;

@Aspect
@Component
public class WorkflowEngineAspect {

    @Around("@annotation(com.tbyacoub.workflow.engine.annotation.PredicateArgumentSize)")
    public Object checkPredicateArgumentSize(ProceedingJoinPoint joinPoint) throws Throwable {
        int size = ((MethodSignature) joinPoint.getSignature())
                .getMethod()
                .getAnnotation(PredicateArgumentSize.class)
                .size();

        String[] args = (String[]) joinPoint.getArgs()[0];

        if (size != args.length) {
            throw new PredicateException(String.format(PREDICATE_ARGUMENT_SIZE, size));
        }

        return joinPoint.proceed();
    }

    @Around("@annotation(com.tbyacoub.workflow.engine.annotation.PredicateArgumentShape)")
    public Object checkPredicateArgumentShape(ProceedingJoinPoint joinPoint) throws Throwable {
        Class<?>[] shapes = ((MethodSignature) joinPoint.getSignature())
                .getMethod()
                .getAnnotation(PredicateArgumentShape.class)
                .shape();

        String[] args = (String[]) joinPoint.getArgs()[0];

        for (int i = 0; i < args.length; i++) {
            try {
                if (shapes[i] == Integer.class) {
                    Integer.parseInt(args[i]);
                }
            } catch (Exception exception) {
                throw new PredicateException(String.format(PREDICATE_ARGUMENT_SHAPE, Arrays.toString(shapes)));
            }
        }

        return joinPoint.proceed();
    }
}
