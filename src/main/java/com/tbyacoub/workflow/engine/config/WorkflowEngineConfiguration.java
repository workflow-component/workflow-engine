package com.tbyacoub.workflow.engine.config;

import com.tbyacoub.workflow.engine.execute.predicate.JSONArrayPredicates;
import com.tbyacoub.workflow.engine.execute.predicate.ListPredicates;
import com.tbyacoub.workflow.engine.repository.WorkflowStepRepository;
import com.tbyacoub.workflow.engine.service.JpaWorkflowStepService;
import com.tbyacoub.workflow.engine.service.WorkflowStepService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WorkflowEngineConfiguration {

    @Bean
    public WorkflowStepService workflowStepService(
            WorkflowStepRepository workflowStepRepository
    ) {
        return new JpaWorkflowStepService(workflowStepRepository);
    }

    @Bean
    public ListPredicates<?> listPredicates() {
        return new JSONArrayPredicates();
    }
}
