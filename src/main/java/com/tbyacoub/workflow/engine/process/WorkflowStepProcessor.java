package com.tbyacoub.workflow.engine.process;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.tbyacoub.workflow.engine.exception.WorkflowStepExecutionException;
import com.tbyacoub.workflow.engine.execute.WorkflowStepExecutor;
import com.tbyacoub.workflow.engine.execute.predicate.ListPredicates;
import com.tbyacoub.workflow.engine.factory.WorkflowStepExecutorFactory;
import com.tbyacoub.workflow.engine.model.WorkflowStep;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Component
public class WorkflowStepProcessor {

    private final ListPredicates<?> listPredicates;

    public WorkflowStepProcessor(ListPredicates<?> listPredicates) {
        this.listPredicates = listPredicates;
    }

    public WorkflowStepProcessorResult process(@NotEmpty String object, @Validated WorkflowStep workflowStep) {
        WorkflowStepProcessorResult.Builder resultBuilder = new WorkflowStepProcessorResult.Builder();

        Object field;
        try {
            field = JsonPath.parse(object).read(workflowStep.getFieldPath());
        } catch (PathNotFoundException pathNotFoundException) {
            return resultBuilder
                    .setResult(false)
                    .setType(WorkflowStepProcessorResult.Type.ERROR)
                    .withFieldValue(null)
                    .withErrorMessage(pathNotFoundException.getMessage())
                    .build();
        }

        resultBuilder.withFieldValue(field);

        WorkflowStepExecutor<Object> workflowStepExecutor = getStepExecutor(workflowStep.getMetadata(), field);

        try {
            resultBuilder
                    .setResult(workflowStepExecutor.execute(field, workflowStep.getMetadata()))
                    .setType(WorkflowStepProcessorResult.Type.SUCCESS);
        } catch (WorkflowStepExecutionException workflowStepExecutionException) {
            resultBuilder
                    .setType(WorkflowStepProcessorResult.Type.ERROR)
                    .withErrorMessage(workflowStepExecutionException.getMessage());
        }

        return resultBuilder.build();
    }

    @SuppressWarnings("unchecked")
    private WorkflowStepExecutor<Object> getStepExecutor(WorkflowStepMetadata workflowStepMetadata, Object field) {
        WorkflowStepExecutorFactory<?> workflowStepExecutorFactory = WorkflowStepExecutorFactory
                .getInstance(field.getClass(), workflowStepMetadata.getType(), listPredicates);
        return (WorkflowStepExecutor<Object>) workflowStepExecutorFactory.getInstance();
    }
}
