package com.tbyacoub.workflow.engine.process;

import lombok.Getter;

@Getter
public class WorkflowStepProcessorResult {

    public enum Type {
        SUCCESS,
        ERROR
    }

    private Type type;

    private boolean result;

    private Object fieldValue;

    private String errorMessage;

    public static class Builder {

        private final WorkflowStepProcessorResult workflowStepProcessorResult = new WorkflowStepProcessorResult();

        public Builder setType(Type type) {
            workflowStepProcessorResult.type = type;

            return this;
        }

        public Builder setResult(boolean result) {
            workflowStepProcessorResult.result = result;

            return this;
        }

        public Builder withFieldValue(Object fieldValue) {
            this.workflowStepProcessorResult.fieldValue = fieldValue;

            return this;
        }

        public Builder withErrorMessage(String errorMessage) {
            this.workflowStepProcessorResult.errorMessage = errorMessage;

            return this;
        }

        public WorkflowStepProcessorResult build() {
            return workflowStepProcessorResult;
        }
    }
}
