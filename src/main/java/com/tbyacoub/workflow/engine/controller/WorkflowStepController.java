package com.tbyacoub.workflow.engine.controller;

import com.google.protobuf.Empty;
import com.tbyacoub.workflow.engine.converter.WorkflowStepConverter;
import com.tbyacoub.workflow.engine.converter.WorkflowStepMessageConverter;
import com.tbyacoub.workflow.engine.converter.WorkflowStepProcessorResultConverter;
import com.tbyacoub.workflow.engine.converter.WorkflowStepTestMessageConverter;
import com.tbyacoub.workflow.engine.grpc.WorkflowStepGrpc;
import com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.*;
import com.tbyacoub.workflow.engine.model.WorkflowStep;
import com.tbyacoub.workflow.engine.model.WorkflowStepTest;
import com.tbyacoub.workflow.engine.process.WorkflowStepProcessor;
import com.tbyacoub.workflow.engine.process.WorkflowStepProcessorResult;
import com.tbyacoub.workflow.engine.service.WorkflowStepService;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.validation.ConstraintViolationException;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@GrpcService
public class WorkflowStepController extends WorkflowStepGrpc.WorkflowStepImplBase {

    private final WorkflowStepService workflowStepService;

    private final WorkflowStepMessageConverter workflowStepMessageConverter;

    private final WorkflowStepConverter workflowStepConverter;

    private final WorkflowStepProcessor workflowStepProcessor;

    private final WorkflowStepTestMessageConverter workflowStepTestMessageConverter;

    private final WorkflowStepProcessorResultConverter workflowStepProcessorResultConverter;

    public WorkflowStepController(
            WorkflowStepService workflowStepService,
            WorkflowStepMessageConverter workflowStepMessageConverter,
            WorkflowStepConverter workflowStepConverter,
            WorkflowStepProcessor workflowStepProcessor, WorkflowStepTestMessageConverter workflowStepTestMessageConverter, WorkflowStepProcessorResultConverter workflowStepProcessorResultConverter) {
        this.workflowStepService = workflowStepService;
        this.workflowStepMessageConverter = workflowStepMessageConverter;
        this.workflowStepConverter = workflowStepConverter;
        this.workflowStepProcessor = workflowStepProcessor;
        this.workflowStepTestMessageConverter = workflowStepTestMessageConverter;
        this.workflowStepProcessorResultConverter = workflowStepProcessorResultConverter;
    }

    @Override
    @PreAuthorize("hasAuthority('SCOPE_workflow_engine/workflow_step')")
    public void create(WorkflowStepMessage request, StreamObserver<WorkflowStepMessage> responseObserver) {
        WorkflowStep workflowStep = workflowStepMessageConverter.convert(request);

        try {
            responseObserver.onNext(workflowStepConverter.convert(workflowStepService.save(workflowStep)));
            responseObserver.onCompleted();
        } catch (ConstraintViolationException constraintViolationException) {
            responseObserver.onError(Status
                    .INVALID_ARGUMENT
                    .withDescription(constraintViolationException.getMessage())
                    .asRuntimeException()
            );
        }
    }

    @Override
    public void find(FindQuery request, StreamObserver<WorkflowStepMessage> responseObserver) {
        Optional<WorkflowStep> optionalWorkflowStep = workflowStepService.find(request.getId());

        WorkflowStepMessage workflowStepMessage = null;

        if (optionalWorkflowStep.isPresent()) {
            workflowStepMessage = workflowStepConverter.convert(optionalWorkflowStep.get());
        }

        responseObserver.onNext(workflowStepMessage);
        responseObserver.onCompleted();
    }

    @Override
    public void findAll(Empty request, StreamObserver<WorkflowStepMessageList> responseObserver) {
        findAllSharedLogic(
                workflowStepService.findAll(),
                WorkflowStepMessageList.newBuilder(),
                responseObserver
        );
    }

    @Override
    public void findAllById(FindAllByIdQuery request, StreamObserver<WorkflowStepMessageList> responseObserver) {
        findAllSharedLogic(
                workflowStepService.findAllById(request.getIdList()),
                WorkflowStepMessageList.newBuilder(),
                responseObserver
        );
    }

    @Override
    public void testWorkflowStep(WorkflowStepTestMessage request, StreamObserver<WorkflowStepProcessorResultMessage> responseObserver) {
        WorkflowStepTest workflowStepTest = workflowStepTestMessageConverter.convert(request);

        try {
            WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                    Objects.requireNonNull(workflowStepTest).getTestObject(), workflowStepTest.getWorkflowStep()
            );
            responseObserver.onNext(workflowStepProcessorResultConverter.convert(workflowStepProcessorResult));
            responseObserver.onCompleted();
        } catch (ConstraintViolationException | IllegalArgumentException exception) {
            responseObserver.onError(Status
                    .INVALID_ARGUMENT
                    .withDescription(exception.getMessage())
                    .asRuntimeException()
            );
        }
    }


    private void findAllSharedLogic(
            Iterable<WorkflowStep> workflowSteps,
            WorkflowStepMessageList.Builder workflowStepMessageBuilder,
            StreamObserver<WorkflowStepMessageList> responseObserver
    ) {
        workflowSteps.forEach(workflowStep -> workflowStepMessageBuilder
                .addWorkflowStepMessage(workflowStepConverter.convert(workflowStep)));

        responseObserver.onNext(workflowStepMessageBuilder.build());
        responseObserver.onCompleted();
    }
}
