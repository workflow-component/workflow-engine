package com.tbyacoub.workflow.engine.execute.executors;

import com.tbyacoub.workflow.engine.exception.WorkflowStepExecutionException;
import com.tbyacoub.workflow.engine.execute.WorkflowStepExecutor;
import com.tbyacoub.workflow.engine.execute.predicate.ListPredicates;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.function.Predicate;

import static com.tbyacoub.workflow.engine.common.CommonVariables.ListExecutorCommons.ARGUMENT_SEPERATOR;
import static com.tbyacoub.workflow.engine.common.ErrorMessages.WorkflowStepExecution.*;

public class ListExecutor<T> implements WorkflowStepExecutor<T> {

    private final ListPredicates<T> listPredicates;

    public ListExecutor(ListPredicates<T> listPredicates) {
        this.listPredicates = listPredicates;
    }

    @Override
    public boolean execute(T field, WorkflowStepMetadata metadata) {
        if (StringUtils.isEmpty(metadata.getArgument())) {
            throw new WorkflowStepExecutionException(ONE_ARGUMENT_REQUIRED);
        }

        String[] argument = metadata.getArgument().split(ARGUMENT_SEPERATOR);
        String methodName = argument[0];
        Object arguments = Arrays.copyOfRange(argument, 1, argument.length);

        Method method;
        try {
            method = listPredicates.getClass().getMethod(methodName, String[].class);
        } catch (NoSuchMethodException e) {
            throw new WorkflowStepExecutionException(NO_SUCH_METHOD);
        }

        Predicate<T> predicate;

        try {
            //noinspection unchecked,JavaReflectionInvocation,JavaReflectionInvocation
            predicate = (Predicate<T>) method.invoke(listPredicates, arguments);
        } catch (InvocationTargetException invocationTargetException) {
            throw new WorkflowStepExecutionException(invocationTargetException.getTargetException().getMessage());
        } catch (IllegalAccessException | IllegalArgumentException illegalException) {
            throw new WorkflowStepExecutionException(METHOD_INVOCATION_EXCEPTION);
        }

        return predicate.test(field);
    }
}
