package com.tbyacoub.workflow.engine.execute.executors;

import com.tbyacoub.workflow.engine.exception.WorkflowStepExecutionException;
import com.tbyacoub.workflow.engine.execute.WorkflowStepExecutor;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;
import net.minidev.json.JSONArray;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.SpelParseException;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.util.Objects;

import static com.tbyacoub.workflow.engine.common.ErrorMessages.WorkflowStepExecution.EXPRESSION_RESULT_NOT_BOOLEAN;
import static com.tbyacoub.workflow.engine.common.ErrorMessages.WorkflowStepExecution.FIELD_IS_NOT_PRIMITIVE;

public class ExpressionExecutor<T> implements WorkflowStepExecutor<T> {

    @Override
    public boolean execute(T field, WorkflowStepMetadata metadata) {
        if (field instanceof JSONArray) return execute((JSONArray) field, metadata.getArgument());

        return execute(field, metadata.getArgument());
    }

    private boolean execute(T field, String placeholderExpression) {
        String finalExpression = placeholderExpression.replace("${field}", field.toString());

        ExpressionParser parser = new SpelExpressionParser();
        Expression expression;
        try {
            expression = parser.parseExpression(finalExpression);
        } catch (SpelParseException spelParseException) {
            throw new WorkflowStepExecutionException(FIELD_IS_NOT_PRIMITIVE);
        }

        boolean result;

        try {
            result = Objects.requireNonNull(expression.getValue(Boolean.class));
        } catch (SpelEvaluationException spelEvaluationException) {
            throw new WorkflowStepExecutionException(EXPRESSION_RESULT_NOT_BOOLEAN);
        }

        return result;
    }

    private boolean execute(JSONArray fields, String expression) {
        @SuppressWarnings("unchecked") final T[] tFields = (T[]) fields.toArray();

        for (T field : tFields) {
            if (!execute(field, expression)) return false;
        }

        return true;
    }
}
