package com.tbyacoub.workflow.engine.execute.predicate;

import com.tbyacoub.workflow.engine.annotation.PredicateArgumentShape;
import com.tbyacoub.workflow.engine.annotation.PredicateArgumentSize;
import net.minidev.json.JSONArray;

import java.util.ArrayList;
import java.util.function.Predicate;

public class JSONArrayPredicates implements ListPredicates<JSONArray> {

    @Override
    @PredicateArgumentSize(size = 1)
    @PredicateArgumentShape(shape = {Integer.class})
    public Predicate<JSONArray> sizeEqual(String[] arguments) {
        return list -> list.size() == Integer.parseInt(arguments[0]);
    }

    @Override
    @PredicateArgumentSize()
    public Predicate<JSONArray> isEmpty(String[] arguments) {
        return ArrayList::isEmpty;
    }
}
