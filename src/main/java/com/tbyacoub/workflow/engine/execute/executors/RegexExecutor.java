package com.tbyacoub.workflow.engine.execute.executors;

import com.tbyacoub.workflow.engine.exception.WorkflowStepExecutionException;
import com.tbyacoub.workflow.engine.execute.WorkflowStepExecutor;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;
import net.minidev.json.JSONArray;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.regex.Pattern;

import static com.tbyacoub.workflow.engine.common.ErrorMessages.WorkflowStepExecution.NON_STRING_FIELD;
import static com.tbyacoub.workflow.engine.common.ErrorMessages.WorkflowStepExecution.REGEX_ARGUMENT_IS_EMPTY;

@Component(value = "REGEX")
public class RegexExecutor<T> implements WorkflowStepExecutor<T> {

    @Override
    public boolean execute(T field, WorkflowStepMetadata metadata) {
        if (field instanceof JSONArray) return execute((JSONArray) field, metadata.getArgument());

        return execute(field, metadata.getArgument());
    }

    private boolean execute(T field, String regex) {
        if (StringUtils.isEmpty(regex)) throw new WorkflowStepExecutionException(REGEX_ARGUMENT_IS_EMPTY);
        if (!(field instanceof String)) throw new WorkflowStepExecutionException(NON_STRING_FIELD);

        return Pattern.compile(regex).matcher((String) field).matches();
    }

    public boolean execute(JSONArray fields, String regex) {
        @SuppressWarnings("unchecked") final T[] tFields = (T[]) fields.toArray();

        for (T field : tFields) {
            if (!execute(field, regex)) return false;
        }

        return true;
    }
}
