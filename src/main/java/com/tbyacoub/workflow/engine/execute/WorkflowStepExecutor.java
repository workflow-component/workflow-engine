package com.tbyacoub.workflow.engine.execute;

import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;

public interface WorkflowStepExecutor<T> {

    boolean execute(T field, WorkflowStepMetadata metadata);

}
