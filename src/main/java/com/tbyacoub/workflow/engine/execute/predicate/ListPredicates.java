package com.tbyacoub.workflow.engine.execute.predicate;

import java.util.function.Predicate;

@SuppressWarnings("unused")
public interface ListPredicates<T> {

    Predicate<T> sizeEqual(String[] arguments);

    Predicate<T> isEmpty(String[] arguments);
}
