CREATE TABLE workflow_step(
    id INT AUTO_INCREMENT PRIMARY KEY,
    metadata JSON NOT NULL,
    field_path varchar(256) NOT NULL,
    client_id INT NOT NULL
)