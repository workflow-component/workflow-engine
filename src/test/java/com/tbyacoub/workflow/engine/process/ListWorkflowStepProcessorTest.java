package com.tbyacoub.workflow.engine.process;

import com.tbyacoub.workflow.engine.execute.predicate.ListPredicates;
import com.tbyacoub.workflow.engine.factory.EmployeeBuilder;
import com.tbyacoub.workflow.engine.factory.WorkflowStepFactory;
import com.tbyacoub.workflow.engine.model.WorkflowStep;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.util.Arrays;

import static com.tbyacoub.workflow.engine.common.CommonVariables.ListExecutorCommons.ARGUMENT_SEPERATOR;
import static com.tbyacoub.workflow.engine.common.ErrorMessages.WorkflowStepExecution.*;

@SpringBootTest
public class ListWorkflowStepProcessorTest {

    private WorkflowStepProcessor workflowStepProcessor;

    @Autowired
    ListPredicates<?> listPredicates;

    @BeforeEach
    public void setup() {
        workflowStepProcessor = new WorkflowStepProcessor(listPredicates);
    }

    @Test
    public void testListExecutorWithUnknowMethod_result_false_type_no_such_method() {
        WorkflowStep workflowStep = WorkflowStepFactory.createListInstance(
                String.format("unknownMethod%s11", ARGUMENT_SEPERATOR),
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder().build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(NO_SUCH_METHOD, workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
    }

    @Test
    public void testListExecutorWithEmptyArguments_result_false_type_one_argument_required() {
        WorkflowStep workflowStep = WorkflowStepFactory.createListInstance(
                "",
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder().build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(ONE_ARGUMENT_REQUIRED, workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
    }

    @Test
    public void testListExecutorWithNullArguments_result_false_type_one_argument_required() {
        WorkflowStep workflowStep = WorkflowStepFactory.createListInstance(
                null,
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder().build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(ONE_ARGUMENT_REQUIRED, workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
    }

    @Test
    public void testListExecutorWithNoPredicateArgument_result_false_type_predicate_argument_required() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createListInstance(
                "sizeEqual",
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(1, 10)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(
                String.format(PREDICATE_ARGUMENT_SIZE, "1"),
                workflowStepProcessorResult.getErrorMessage()
        );
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
    }

    @Test
    public void testListExecutorWithWrongPrediateArgument_result_false_type_list_predicate_argument_type() {
        WorkflowStep workflowStep = WorkflowStepFactory.createListInstance(
                String.format("sizeEqual%sabc", ARGUMENT_SEPERATOR),
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder().build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(
                String.format(PREDICATE_ARGUMENT_SHAPE, Arrays.toString(new Class<?>[]{Integer.class})),
                workflowStepProcessorResult.getErrorMessage()
        );
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
    }

    @Test
    public void testListExecutorWithListSize_result_true_type_success() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createListInstance(
                String.format("sizeEqual%s11", ARGUMENT_SEPERATOR),
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(1, 10)
                        .build(),
                workflowStep
        );

        Assertions.assertTrue(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
    }

    @Test
    public void testListExecutorWithListSize_result_false_type_success() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createListInstance(
                String.format("sizeEqual%s12", ARGUMENT_SEPERATOR),
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(1, 10)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
    }

    @Test
    public void testListExecutorWithListIsEmpty_result_false_type_success() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createListInstance(
                "isEmpty",
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(1, 10)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
    }

    @Test
    public void testListExecutorWithListIsEmpty_result_true_type_success() {
        WorkflowStep workflowStep = WorkflowStepFactory.createListInstance(
                "isEmpty",
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .clearSeverity()
                        .build(),
                workflowStep
        );

        Assertions.assertTrue(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
    }
}


