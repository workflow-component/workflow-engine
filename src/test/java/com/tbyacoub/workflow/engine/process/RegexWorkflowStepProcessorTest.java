package com.tbyacoub.workflow.engine.process;

import com.tbyacoub.workflow.engine.execute.predicate.ListPredicates;
import com.tbyacoub.workflow.engine.factory.EmployeeBuilder;
import com.tbyacoub.workflow.engine.factory.WorkflowStepFactory;
import com.tbyacoub.workflow.engine.model.WorkflowStep;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;

import static com.tbyacoub.workflow.engine.common.ErrorMessages.WorkflowStepExecution.NON_STRING_FIELD;

@SpringBootTest
public class RegexWorkflowStepProcessorTest {

    private WorkflowStepProcessor workflowStepProcessor;

    @Autowired
    ListPredicates<?> listPredicates;

    @BeforeEach
    public void setup() {
        workflowStepProcessor = new WorkflowStepProcessor(listPredicates);
    }

    @Test
    public void testRegexExecutorWithNonStringObject_result_false_type_non_string_field() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createRegexInstance(
                "^[a-zA-Z]+$",
                "$.violations[0].severity"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(1, 0)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(NON_STRING_FIELD, workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
    }

    @Test
    public void testRegexExecutorWithNonStringListObject_result_false_type_non_string_field() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createRegexInstance(
                "^[a-zA-Z]+$",
                "$.violations..severity"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(1, 0)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(NON_STRING_FIELD, workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
    }

    @Test
    public void testRegexExecutorWithString_result_true_type_success() {
        @SuppressWarnings("SpellCheckingInspection") String employeeName = "thisisaname";

        WorkflowStep workflowStep = WorkflowStepFactory.createRegexInstance(
                "^[a-zA-Z]+$",
                "$.name"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withName(employeeName)
                        .build(),
                workflowStep
        );

        Assertions.assertTrue(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(employeeName, workflowStepProcessorResult.getFieldValue());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
    }

    @Test
    public void testRegexExecutorWithStringList_result_true_type_success() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createRegexInstance(
                "^\\d{1,2}\\/\\d{1,2}\\/\\d{4}$",
                "$.violations..violation_date"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(0, 1)
                        .build(),
                workflowStep
        );

        Assertions.assertTrue(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
    }
}
