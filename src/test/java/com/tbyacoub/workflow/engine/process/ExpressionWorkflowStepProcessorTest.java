package com.tbyacoub.workflow.engine.process;

import com.tbyacoub.workflow.engine.common.ErrorMessages;
import com.tbyacoub.workflow.engine.execute.predicate.ListPredicates;
import com.tbyacoub.workflow.engine.factory.EmployeeBuilder;
import com.tbyacoub.workflow.engine.factory.WorkflowStepFactory;
import com.tbyacoub.workflow.engine.model.Employee;
import com.tbyacoub.workflow.engine.model.WorkflowStep;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;

@SpringBootTest
public class ExpressionWorkflowStepProcessorTest {

    private WorkflowStepProcessor workflowStepProcessor;

    @Autowired
    ListPredicates<?> listPredicates;

    @BeforeEach
    public void setup() {
        workflowStepProcessor = new WorkflowStepProcessor(listPredicates);
    }

    @Test
    public void testExpressionExecutorWithStringLength_result_true_type_success() {
        WorkflowStep workflowStep = WorkflowStepFactory.createExpressionInstance(
                String.format("'${field}'.length() == %s", Employee.Level.ONE.name().length()),
                "$.level"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withEmployeeLevel(Employee.Level.ONE)
                        .build(),
                workflowStep
        );

        Assertions.assertTrue(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
        Assertions.assertEquals(
                workflowStepProcessorResult.getFieldValue(),
                Employee.Level.ONE.name()
        );

        workflowStep.getMetadata().setArgument(
                String.format("'${field}'.length() == %s", Employee.Level.MANAGER.name().length())
        );

        workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withEmployeeLevel(Employee.Level.MANAGER)
                        .build(),
                workflowStep
        );

        Assertions.assertTrue(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
        Assertions.assertEquals(
                workflowStepProcessorResult.getFieldValue(),
                Employee.Level.MANAGER.name()
        );
    }

    @Test
    public void testExpressionExecutorWithStringLength_result_false_type_success() {
        WorkflowStep workflowStep = WorkflowStepFactory.createExpressionInstance(
                "'${field}'.length() == 1",
                "$.level"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder().build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
    }

    @Test
    public void testExpressionExecutorWithIntegerComparision_result_true_type_success() throws ParseException {
        int severity = 5;
        int violationIndex = 0;

        WorkflowStep workflowStep = WorkflowStepFactory.createExpressionInstance(
                "${field} > 4",
                String.format("$.violations[%s].severity", violationIndex)
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(severity, violationIndex)
                        .build(),
                workflowStep
        );

        Assertions.assertTrue(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
        Assertions.assertEquals(workflowStepProcessorResult.getFieldValue(), severity);
    }

    @Test
    public void testExpressionExecutorWithIntegerComparision_result_false_type_success() throws ParseException {
        int severity = 5;
        int violationIndex = 0;

        WorkflowStep workflowStep = WorkflowStepFactory.createExpressionInstance(
                "${field} > 5",
                String.format("$.violations[%s].severity", violationIndex)
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(severity, violationIndex)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
        Assertions.assertEquals(workflowStepProcessorResult.getFieldValue(), severity);
    }

    @Test
    public void testExpressionExecutorWithAllIntegerComparision_result_true_type_success() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createExpressionInstance(
                "${field} <= 10",
                "$.violations..severity"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(9, 6)
                        .build(),
                workflowStep
        );

        Assertions.assertTrue(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
    }

    @Test
    public void testExpressionExecutorWithAllIntegerComparision_result_false_type_success() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createExpressionInstance(
                "${field} <= 1",
                "$.violations..severity"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(3, 6)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertNull(workflowStepProcessorResult.getErrorMessage());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.SUCCESS, workflowStepProcessorResult.getType());
    }

    @Test
    public void testExpressionExecutorWithJSONArray_type_error_field_is_not_primitive() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createExpressionInstance(
                "${field}.size() == 0",
                "$.violations"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(3, 6)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
        Assertions.assertEquals(
                workflowStepProcessorResult.getErrorMessage(),
                ErrorMessages.WorkflowStepExecution.FIELD_IS_NOT_PRIMITIVE
        );
    }

    @Test
    public void testExpressionExecutorWithString_type_expression_result_not_boolean() throws ParseException {
        WorkflowStep workflowStep = WorkflowStepFactory.createExpressionInstance(
                "'${field}'",
                "$.name"
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(3, 6)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
        Assertions.assertEquals(
                workflowStepProcessorResult.getErrorMessage(),
                ErrorMessages.WorkflowStepExecution.EXPRESSION_RESULT_NOT_BOOLEAN
        );
    }

    @Test
    public void testExpressionExecutorWithString_type_not_results_for_path() throws ParseException {
        String nonExistentPath = "pathDoesNotExistent";
        WorkflowStep workflowStep = WorkflowStepFactory.createExpressionInstance(
                "'${field}'",
                String.format("$.%s", nonExistentPath)
        );

        WorkflowStepProcessorResult workflowStepProcessorResult = workflowStepProcessor.process(
                new EmployeeBuilder()
                        .withSeverity(3, 6)
                        .build(),
                workflowStep
        );

        Assertions.assertFalse(workflowStepProcessorResult.isResult());
        Assertions.assertEquals(WorkflowStepProcessorResult.Type.ERROR, workflowStepProcessorResult.getType());
        Assertions.assertEquals(
                workflowStepProcessorResult.getErrorMessage(),
                String.format("No results for path: $['%s']", nonExistentPath)
        );
    }
}