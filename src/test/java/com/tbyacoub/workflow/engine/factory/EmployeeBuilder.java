package com.tbyacoub.workflow.engine.factory;

import com.github.javafaker.Faker;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tbyacoub.workflow.engine.model.Employee;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class EmployeeBuilder {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private final JsonObject prototype = prototype();

    public EmployeeBuilder withSeverity(int severity, int violationIndex) throws ParseException {
        JsonArray violations = prototype.getAsJsonArray("violations");

        while (violationIndex >= violations.size()) {
            Date esd = DATE_FORMAT.parse(prototype.getAsJsonPrimitive(
                    "employment_start_date"
            ).getAsString());
            violations.add(createViolationInstance(esd));
        }

        violations.get(violationIndex).getAsJsonObject().addProperty("severity", severity);
        prototype.add("violations", violations);

        return this;
    }

    public EmployeeBuilder withEmployeeLevel(Employee.Level employeeLevel) {
        prototype.addProperty("level", employeeLevel.name());

        return this;
    }

    public EmployeeBuilder withName(String name) {
        prototype.addProperty("name", name);

        return this;
    }

    public EmployeeBuilder clearSeverity() {
        prototype.add("violations", new JsonArray());

        return this;
    }

    public String build() {
        return prototype.toString();
    }


    private static JsonObject prototype() {
        Faker faker = new Faker();
        String name = faker.name().firstName();
        int randomViolationCount = faker.number().numberBetween(0, 10);
        Date employment_start_date = faker.date().past(300, TimeUnit.DAYS);

        JsonArray violations = new JsonArray();

        for (int i = 0; i < randomViolationCount; i++) {
            violations.add(createViolationInstance(employment_start_date));
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("email", String.format("%s@gmail.com", name));
        jsonObject.addProperty("employment_start_date", DATE_FORMAT.format(employment_start_date));
        jsonObject.add("violations", violations);
        int index = faker.number().numberBetween(0, Employee.Level.values().length);
        jsonObject.addProperty("level", Employee.Level.values()[index].name());

        return jsonObject;
    }

    private static JsonObject createViolationInstance(Date employment_start_date) {
        Faker faker = new Faker();

        JsonObject violation = new JsonObject();
        violation.addProperty("description", faker.lorem().sentence());
        violation.addProperty("violation_date", DATE_FORMAT.format(faker.date().between(
                employment_start_date,
                new Date()
        )));
        violation.addProperty("severity", faker.number().numberBetween(0, 10));

        return violation;
    }
}
