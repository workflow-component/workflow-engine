package com.tbyacoub.workflow.engine.factory;

import com.tbyacoub.workflow.engine.execute.WorkflowStepExecutor;
import com.tbyacoub.workflow.engine.execute.executors.ExpressionExecutor;
import com.tbyacoub.workflow.engine.execute.executors.ListExecutor;
import com.tbyacoub.workflow.engine.execute.executors.RegexExecutor;
import com.tbyacoub.workflow.engine.execute.predicate.ListPredicates;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WorkflowStepExecutorFactoryTest {

    @Autowired
    ListPredicates<?> listPredicates;

    @Test
    public void ListExpressionExecutor() {
        WorkflowStepExecutorFactory<?> workflowStepExecutorFactory =
                WorkflowStepExecutorFactory.getInstance(JSONArray.class, WorkflowStepMetadata.Type.EXPRESSION, listPredicates);

        WorkflowStepExecutor<?> workflowStepExecutor = workflowStepExecutorFactory.getInstance();

        Assertions.assertSame(workflowStepExecutorFactory.getClazz(), JSONArray.class);
        Assertions.assertEquals(workflowStepExecutor.getClass(), ExpressionExecutor.class);
    }

    @Test
    public void StringExpressionExecutor() {
        WorkflowStepExecutorFactory<?> workflowStepExecutorFactory =
                WorkflowStepExecutorFactory.getInstance(String.class, WorkflowStepMetadata.Type.EXPRESSION, listPredicates);

        WorkflowStepExecutor<?> workflowStepExecutor = workflowStepExecutorFactory.getInstance();

        Assertions.assertSame(workflowStepExecutorFactory.getClazz(), String.class);
        Assertions.assertEquals(workflowStepExecutor.getClass(), ExpressionExecutor.class);
    }

    @Test
    public void IntegerExpressionExecutor() {
        WorkflowStepExecutorFactory<?> workflowStepExecutorFactory =
                WorkflowStepExecutorFactory.getInstance(Integer.class, WorkflowStepMetadata.Type.EXPRESSION, listPredicates);

        WorkflowStepExecutor<?> workflowStepExecutor = workflowStepExecutorFactory.getInstance();

        Assertions.assertSame(workflowStepExecutorFactory.getClazz(), Integer.class);
        Assertions.assertEquals(workflowStepExecutor.getClass(), ExpressionExecutor.class);
    }

    @Test
    public void DoubleExpressionExecutor() {
        WorkflowStepExecutorFactory<?> workflowStepExecutorFactory =
                WorkflowStepExecutorFactory.getInstance(Double.class, WorkflowStepMetadata.Type.EXPRESSION, listPredicates);

        WorkflowStepExecutor<?> workflowStepExecutor = workflowStepExecutorFactory.getInstance();

        Assertions.assertSame(workflowStepExecutorFactory.getClazz(), Double.class);
        Assertions.assertEquals(workflowStepExecutor.getClass(), ExpressionExecutor.class);
    }

    @Test
    public void JSONArrayListExecutor() {
        WorkflowStepExecutorFactory<?> workflowStepExecutorFactory =
                WorkflowStepExecutorFactory.getInstance(JSONArray.class, WorkflowStepMetadata.Type.LIST, listPredicates);


        WorkflowStepExecutor<?> workflowStepExecutor = workflowStepExecutorFactory.getInstance();

        Assertions.assertSame(workflowStepExecutorFactory.getClazz(), JSONArray.class);
        Assertions.assertEquals(workflowStepExecutor.getClass(), ListExecutor.class);
    }

    @Test
    public void RegexExecutor() {
        WorkflowStepExecutorFactory<?> workflowStepExecutorFactory =
                WorkflowStepExecutorFactory.getInstance(String.class, WorkflowStepMetadata.Type.REGEX, listPredicates);


        WorkflowStepExecutor<?> workflowStepExecutor = workflowStepExecutorFactory.getInstance();

        Assertions.assertSame(workflowStepExecutorFactory.getClazz(), String.class);
        Assertions.assertEquals(workflowStepExecutor.getClass(), RegexExecutor.class);
    }
}
