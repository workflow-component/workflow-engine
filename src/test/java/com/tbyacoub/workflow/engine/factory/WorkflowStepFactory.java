package com.tbyacoub.workflow.engine.factory;

import com.tbyacoub.workflow.engine.model.WorkflowStep;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;

public class WorkflowStepFactory {

    public static WorkflowStep createExpressionInstance(String argument, String fieldPath) {
        return defaultInstance(WorkflowStepMetadata.Type.EXPRESSION, argument, fieldPath);
    }

    public static WorkflowStep createListInstance(String argument, String fieldPath) {
        return defaultInstance(WorkflowStepMetadata.Type.LIST, argument, fieldPath);
    }

    public static WorkflowStep createRegexInstance(String argument, String fieldPath) {
        return defaultInstance(WorkflowStepMetadata.Type.REGEX, argument, fieldPath);
    }

    private static WorkflowStep defaultInstance(WorkflowStepMetadata.Type type, String argument, String fieldPath) {
        WorkflowStepMetadata workflowStepMetadata = new WorkflowStepMetadata();
        workflowStepMetadata.setType(type);
        workflowStepMetadata.setArgument(argument);

        WorkflowStep workflowStep = new WorkflowStep();
        workflowStep.setMetadata(workflowStepMetadata);
        workflowStep.setFieldPath(fieldPath);
        workflowStep.setClientId(0L);

        return workflowStep;
    }
}
