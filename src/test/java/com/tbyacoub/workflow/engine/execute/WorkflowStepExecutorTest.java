package com.tbyacoub.workflow.engine.execute;

import com.tbyacoub.workflow.engine.execute.executors.ExpressionExecutor;
import com.tbyacoub.workflow.engine.model.WorkflowStepMetadata;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WorkflowStepExecutorTest {


    @Test
    public void StringExpressionExecutor() {
        WorkflowStepExecutor<String> workflowStepExecutor = new ExpressionExecutor<>();

        WorkflowStepMetadata workflowStepMetadata = new WorkflowStepMetadata();
        workflowStepMetadata.setArgument("'${field}'.length() == 4");

        boolean result = workflowStepExecutor.execute("test", workflowStepMetadata);

        Assertions.assertTrue(result);
    }

    @Test
    public void IntegerExpressionExecutor() {
        WorkflowStepExecutor<Integer> workflowStepExecutor = new ExpressionExecutor<>();

        WorkflowStepMetadata workflowStepMetadata = new WorkflowStepMetadata();
        workflowStepMetadata.setArgument("${field} == 4");

        boolean result = workflowStepExecutor.execute(4, workflowStepMetadata);

        Assertions.assertTrue(result);
    }

    @Test
    public void DoubleExpressionExecutor() {
        WorkflowStepExecutor<Double> workflowStepExecutor = new ExpressionExecutor<>();

        WorkflowStepMetadata workflowStepMetadata = new WorkflowStepMetadata();
        workflowStepMetadata.setArgument("4 == T(java.lang.Math).floor(${field})");

        boolean result = workflowStepExecutor.execute(4.3, workflowStepMetadata);

        Assertions.assertTrue(result);
    }

    @Test
    public void StringListExpressionExecutor() {
        WorkflowStepExecutor<JSONArray> workflowStepExecutor = new ExpressionExecutor<>();

        JSONArray stringList = new JSONArray();
        stringList.add("test1");
        stringList.add("test2");
        stringList.add("test3");

        WorkflowStepMetadata workflowStepMetadata = new WorkflowStepMetadata();
        workflowStepMetadata.setArgument("'${field}'.length() == 5");

        boolean result = workflowStepExecutor.execute(stringList, workflowStepMetadata);

        Assertions.assertTrue(result);
    }

    @Test
    public void IntegerListExpressionExecutor() {
        WorkflowStepExecutor<JSONArray> workflowStepExecutor = new ExpressionExecutor<>();

        JSONArray stringList = new JSONArray();
        stringList.add(1);
        stringList.add(2);
        stringList.add(3);

        WorkflowStepMetadata workflowStepMetadata = new WorkflowStepMetadata();
        workflowStepMetadata.setArgument("${field} < 5");

        boolean result = workflowStepExecutor.execute(stringList, workflowStepMetadata);

        Assertions.assertTrue(result);
    }
}
